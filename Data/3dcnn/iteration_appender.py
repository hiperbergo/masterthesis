import random
import sys
fname=str(sys.argv[1])
with open(fname, 'r') as f:
    lines = f.readlines()
for i in range (0, len(lines)):
    lines[i] = str(i*100) + ' ' + lines[i]
with open(fname, 'w') as f:
    f.writelines(lines)
