\begin{tikzpicture}
	\tikzset{
		cube/.style={thick, draw=ualightblue, fill=white},
		line/.style={thick, draw=uablue50, line cap=round}}
	\node[cube, minimum height=16 em, minimum width=22 em] (D) at (2.25,1.5) {};
	\node[cube, minimum height=16 em, minimum width=22 em] (B) at (1.5,1) {};
	\node[cube, minimum height=16 em, minimum width=22 em] (G) at (0.75,0.5) {};
	\node[cube, minimum height=16 em, minimum width=22 em] (R) at (0,0) {};
%
	\node[cube, very thick, draw=none, fill=none, minimum height=8 em, minimum width=8 em] (C1) at  ([xshift=4.1em, yshift=-4.1em]R.north west) {};
	\node[cube, very thick, draw=none, fill=none, minimum height=8.15 em, minimum width=8 em] (C2) at  ([xshift=4.1em, yshift=-4.16em]D.north west) {};
	\draw[line] (C2.north east) -- (C2.north west);
	\draw[line] (C2.north east) -- (C2.south east);
	\draw[line, dashed] (C2.south west) -- (C2.south east);
	\draw[line, dashed] (C2.south west) -- (C2.north west);
	\draw[line] (C1.north east) -- (C1.north west);
	\draw[line] (C1.north east) -- (C1.south east);
	\draw[line] (C1.south west) -- (C1.south east);
	\draw[line] (C1.south west) -- (C1.north west);
%
	\draw[line] (C1.north east) -- (C2.north east);
	\draw[line] (C1.north west) -- (C2.north west);
	\draw[line] (C1.south east) -- (C2.south east);
	\draw[line, dashed] (C1.south west) -- (C2.south west);
%
	\node[inner sep=0.2em, fill=uablue30] at (C1.center) {};
	\draw[line, line width=0.2em, uablue30, -latex] (C1.center) -- ([xshift=15em]C1.center);
	\draw[line, line width=0.2em, uablue30, -latex] (C1.center) -- ([yshift=-10em]C1.center);
%
	\node[ualightblue] at ([xshift=-1em, yshift=1em]R.north west) {R};
	\node[ualightblue] at ([xshift=-1em, yshift=1em]G.north west) {G};
	\node[ualightblue] at ([xshift=-1em, yshift=1em]B.north west) {B};
	\node[ualightblue] at ([xshift=-1em, yshift=1em]D.north west) {D};
%
	\node[cube, minimum height=12em, minimum width=16 em] (RES) at ([xshift=20em]G.east) {};
	\foreach \i in {1,...,16}
		\node[cube, draw =none, opacity=0.75, minimum height=2em, minimum width=2em, fill=uablue30] at ([xshift=12 em + rnd*15 em, yshift=-5 em + rnd*10 em]G.east) {};
	\foreach \i in {1,...,16}
		\node[cube, draw=none, opacity=0.75, minimum height=2em, minimum width=2em, fill=uablue10] at ([xshift=13 em + rnd*14 em, yshift=-5 em + rnd*10 em]G.east) {};
	\foreach \i in {1,...,16}
		\node[cube, draw=none, opacity=0.75, minimum height=2em, minimum width=2em, fill=ualightblue] at ([xshift=13 em + rnd*14 em, yshift=-5 em + rnd*10 em]G.east) {};
%
	\draw[line, line width=0.2em, -latex] ([xshift=1em]G.east) -- ([xshift=-1em]RES.west);
%
	\node[above = 1 em of D] {\larger RGB-D input: ($256\times256)\times4$ channels};
	\node[above = 1 em of RES] {\larger 2D feature map};
%
	\node[] at ([yshift=-1em]R.center) {\larger convolution filter};
\end{tikzpicture}
