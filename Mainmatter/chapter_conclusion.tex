\chapter{Conclusion}
\label{cha:conclusion}

\begin{chapterabstract}
This chapter discusses the main conclusions extracted from the work presented in this Thesis. The chapter is organized in three sections: Section \ref{sec:conclusion:conclusions} presents and discusses the final conclusion of the work presented in this document. Next, Section \ref{sec:conclusion:publications} lists the publications derived from the presented work. Finally, Section \ref{sec:conclusion:future_work} presents future works: open problems and topics that remain for further research.
\end{chapterabstract}

\section{Conclusions}
\label{sec:conclusion:conclusions}

In this Thesis, we first reviewed the state of the art of object recognition methods. After that study, we can state that deep learning is surpassing traditional pipelines based on hand-crafted descriptors. In addition, we observed that \acs{3D}-based methods usually outperform \acs{2D}-based ones thanks to the additional dimension of information. This review laid down the motivation for this work: explore the possibilities of \acs{3D} object class recognition using \acp{CNN}.

After that, we carried out a detailed study about the theoretical background of \acp{CNN}. We introduced the intuitive ideas behind that kind of network, and the differences between \acp{CNN} and fully connected ones. Furthermore, we described the basic architecture of a \acs{CNN} and discussed training theory, considerations, and best practices that would be later applied in the implemented system.

As a prerequisite before diving deep into design and implementation phases, we conducted a study to select essential materials and methods for our work. In that regard, we selected ModelNet as the \acs{3D} object dataset to train our network, Caffe as the framework to develop our system, and we assembled and configured Asimov, a server to provide hardware support to this work.

Next, we discussed how to transform \acs{3D} data into volumetric representations with grid-like structures to feed \acp{CNN}. We reviewed existing proposals and stated what characterizes a good representation. After that study, we proposed several methods to represent \acs{3D} data as \acs{3D} tensors of real-valued components. In addition, we showed how to convert the dataset of choice to those representations simulating real-world sensor data. We also carried out a brief experimentation to analyze the efficiency and scaling of the proposed methods. On the one hand, there was no significant difference between the grid spawning methods. On the other hand, a tradeoff was detected between the information considered by the representation and time needed to compute it. We determined that the density-based representation offered a good balance in both terms, efficiency and quality.

At last, we described the difference between \acs{2D}, \acs{2.5D}, and \acs{3D} \acp{CNN}. We stated that a significant gain was expected from \acs{3D} convolutions with respect to \acs{2.5D} ones since the filters are also convolved in the depth dimension. We then designed, implemented, and tested a \acs{2.5D} \acs{CNN} using the proposed volumetric representations. An in-depth study was carried out to determine their performance in adverse scenarios (noise and occlusions). The adaptive grid outperformed the fixed one, whilst the binary occupancy measure obtained better accuracy than the normalized density one -- the surface triangulation method was excluded due to its prohibitive cost. In the end, a \acs{2.5D} \acs{CNN} using adaptive binary grids of $64\times64\times64$ voxels achieved a $85\%$ success rate when classifying object classes in the ModelNet10 dataset. In addition, that network showed significant robustness against low levels of noise and occlusion that characterize real-world scenarios. However, the final accuracy was limited due to overfitting. In the end, that successful network was extended to support \acs{3D} convolutions and trained again using the dataset with adaptive binary grids. The results showed that the \acs{3D} \acs{CNN} was significantly harder to train than its \acs{2.5D} counterpart. In fact, its inherent complexity aggravated the overfitting problem so the network was not able to achieve good recognition rates.

\section{Publications}
\label{sec:conclusion:publications}

The following publication was achieved as a result of the research carried out during the Master's Thesis:

\emph{A. Garcia-Garcia, F. Gomez-Donoso, J. Garcia-Rodriguez, S. Orts\-Escolano, M. Cazorla, J. Azorin-Lopez}. \textbf{PointNet: A 3D Convolutional Neural Network for Real-Time Object Class Recognition}. International Joint Conference on Neural Networks, IJCNN 2016, Vancouver, Canada.

\section{Future Work}
\label{sec:conclusion:future_work}

Due to the time constraints imposed on this project, many possible improvements and ideas were left out for future works. Here we summarize them to conclude this Thesis.

\begin{itemize}
	\item The surface triangulation method was left out of the experimentation due to its prohibitive computational cost. Improving its efficiency by means of redesigning the algorithmic structure or developing a parallel implementation could allow us to experiment with that representation that might outperform the binary and density ones.
	\item In the same way, improving the efficiency of the binary and density representations with a parallel implementation using \acs{CUDA} unlock its potential application to real-time problems.
	\item In order to avoid overfitting, the dataset must be augmented introducing noise, and variations of the existing models (rotations, translations, crops). Then the same architecture can be tested and check if that reduces overfitting.
	\item Preliminary experiments were carried out using a view-based \acs{3D} \acs{CNN} instead of a model-based one. However, the initial results were not good enough. Determining the source of the problem for that approach and tackling it might provide better results than a model-based architecture. What is more, it could be directly applied to real-world data.
	\item The current system can be included in a full recognition pipeline, taking scenes from an \acs{RGB-D} sensors, segmenting the objects, and applying the network to recognize them.
\end{itemize}
