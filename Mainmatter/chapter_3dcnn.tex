\chapter{\acs{3D} \acs{CNN} for Object Recognition}
\label{cha:3dcnn_object_recognition}

\begin{chapterabstract}
	In this chapter we gather all the knowledge and conclusions extracted from previous ones to design, implement, and train a \acs{3D} \acs{CNN}. In Section \ref{sec:3dcnn_object_recognition:introduction}, we will first introduce the concept of \acs{2.5D} and \acs{3D} \acp{CNN} and why do we expect a gain by moving from \acs{2D} to \acs{3D}. After that, in Section \ref{sec:3dcnn_object_recognition:related_works}, we will explore the related works about \acs{2.5D} \acp{CNN} and its evolution to \acs{3D} \acp{CNN}. Next, in Section \ref{sec:3dcnn_object_recognition:model_based}, we will design, implement, and train a \acs{2.5D} and a \acs{3D} \acp{CNN} using full object models. We will also conduct a detailed study of the effect of noise and occlusion over the accuracy of the networks. At last, we will draw conclusions about the experiments in Section \ref{sec:3dcnn_object_recognition:conclusion}.

\end{chapterabstract}

\section{Introduction}
\label{sec:3dcnn_object_recognition:introduction}

In the previous chapters, we showed how \acp{CNN} are able to recognize objects in \acs{2D} images by learning convolution filters to detect features. We also introduced the possibility of adding a new dimension to this problem by using \acs{3D} data instead of plain images. In order to do that, we stated that we need a way to encode that \acs{3D} information into a volumetric and structured representation that can be provided as input to the \acs{CNN}. We also designed and implemented a set of representations after reviewing the state of the art. Those representations were tested to determine their performance and scalability. All of that provided us with all we need to start designing, implementing, and training \acs{3D} \acp{CNN}. But before diving deep into that, we will first lay down the differences between \acs{2D} \acp{CNN} and \acs{3D} ones, using \acs{2.5D} \acp{CNN} as an intermediate step.
\begin{figure}[!b]
	\centering
	\resizebox{0.75\textwidth}{!}{
		\input{Figures/3dcnn/2d_convolution.tikz}}
		\caption{Applying a \acs{2D} convolution to a single-channel \acs{2D} input, in this case a grayscale image, results in a \acs{2D} feature map. The filter (in dark blue) has a fixed width and height and it is slided across the width and height of the input image, producing a \acs{2D} feature map as a result of the matrix multiplications of the filter and the input.}
	\label{fig:2d_convolution}
\end{figure}

\subsection{\acs{2.5D} \aclp{CNN}}
\label{subsec:3dcnn_object_recognition:introduction:25dcnn}

Figure \ref{fig:2d_convolution} shows an example of applying a \acs{2D} convolution to an image with a single channel. In that case, sliding the kernel over the whole image as described in Chapter \ref{cha:cnn} produces a \acs{2D} feature map. However, most images do not have a single channel but three (usually red, green, and blue). In that case, the filter is extended through all channels but it is still slided across the width and height of the input volume, not across the channels or depth.

\begin{figure}[!t]
	\centering
	\resizebox{0.75\textwidth}{!}{
		\input{Figures/3dcnn/25d_convolution.tikz}}
		\caption{Applying a \acs{2D} convolution to a multi-channel input, in this case \acs{RGB-D} with four channels, results in a \acs{2D} feature map. The filter (in dark blue) has a fixed width and height, but it extends through the full depth of the input volume. During the forward pass, the filter is slided across the width and height of the input volume, producing a \acs{2D} activation or feature map.}
	\label{fig:25d_convolution}
\end{figure}

\begin{figure}[!b]
	\centering
	\resizebox{0.75\textwidth}{!}{
		\input{Figures/3dcnn/3d_convolution.tikz}}
		\caption{Applying a \acs{3D} convolution to a single-channel volumetric input, in this case a $256\times64\times32$ grid, results in a\acs{3D} feature map. The filter (in dark blue) has a fixed width, height, and depth. During the forward pass, the filter is slided across the width, height, and depth of the input volume, producing a \acs{3D} activation map.}
	\label{fig:3d_convolution}
\end{figure}

This behavior can be exploited to feed the \acs{CNN} with \acs{2.5D} data by adding a depth channel to a common \acs{RGB} image, creating an \acs{RGB-D} input volume with four channels. The convolution filter will then be extended through the four channels, creating a somewhat volumetric kernel as shown in Figure \ref{fig:25d_convolution}. Furthermore, we could take a volumetric representation like the ones proposed in Chapter \ref{cha:volumetric_representations} and slice it in the depth dimension into channels. Then we could feed that representation directly to a \acs{CNN}. However, this approach will not slide the filter across the depth of the input volume.

\subsection{\acs{3D} \aclp{CNN}}
\label{subsec:3dcnn_object_recognition:introduction:3dcnn}

On the other hand, \acs{3D} convolutions use kernels with fixed width, height, and depth. That filters are slided across the width, height, and depth of an input volume. By doing that, \acs{3D} convolutions model spatial information better than \acs{2.5D} counterparts which only extend the kernel over the depth dimension but do not slide it over that dimension thus losing spatial information. As shown in Figure \ref{fig:3d_convolution}, a \acs{3D} convolution is applied to a single-channel input volume producing  a volumetric feature map.

\section{Related Works}
\label{sec:3dcnn_object_recognition:related_works}

After clarifying the differences between \acs{2D}, \acs{2.5D}, and \acs{3D} convolutions, we will review the literature to analyze state-of-the-art \acs{2.5D} and \acs{3D} approaches. Due to the successful applications of \acp{CNN} to \acs{2D} image analysis, several researchers decided to increase the dimensionality of the input by adding depth information as an additional channel to conform \acs{2.5D} \acp{CNN}.

Socher \emph{et al.} \cite{Socher2012} introduced a model based on a combination of \acp{CNN} and \acp{RNN} to learn features and classify \acs{RGB-D} images. That model aims to learn low-level and translation invariant features with the \acs{CNN} layers, those features are then given as inputs to fixed-tree \acp{RNN} to compose higher order features. Alexandre \emph{et al.} \cite{Alexandre2014} explore the possibility of transferring knowledge \cite{Pan2010}\cite{Ciresan2012} between \acp{CNN} to improve accuracy and reducing training time when classifying \acs{RGB-D} data. Hoeft \emph{et al.} \cite{Hoeft2014} proposed a four-stage \acs{CNN} architecture, derived from the work of Schulz and Behnke \cite{Schulz2012}, to semantically segment \acs{RGB-D} scenes, providing the depth channel as feature maps representing components of a simplified histogram of oriented depth operator. Wang \emph{et al.} \cite{Wang2015} combined a \acs{CNN}, to extract representative image features from \acs{RGB-D}, with a \ac{SVM} to classify objects in those images. Schwarz \emph{et al.} \cite{Schwarz2015} went one step beyond. They presented a system for object recognition and pose estimation using \acs{RGB-D} images and transfer learning between a pre-trained \acs{CNN} for image categorization and another \acs{CNN} to classify colorized depth images. The features are then classified into instances and categories by \acp{SVM} and the pose is estimated via using another \acs{RBF} kernel \acs{SVM}.

In spite of the fact that those methods extend the traditional \ac{CNN}, they do not employ a pure volumetric representation and therefore they do not make full use of the geometric information in the data. What is more, they do not use \acs{3D} convolutions. This is why they fall in the \acs{2.5D} \acp{CNN} category. In order to improve \acs{2.5D} \acp{CNN}, several authors proposed pure volumetric approaches or the so called \acs{3D} \acp{CNN}. These architectures apply spatially \acs{3D} convolutions fully utilizing geometric data.

The seminal work of Wu \emph{et al.} \cite{Wu2015} introduced a system that supports joint object recognition and shape completion from \acs{2.5D} depth maps that are transformed into a \acs{3D} shape representation which consists of a probability distribution of binary values on a \acs{3D} voxel grid. A \ac{CDBN} is used to recognize categories, complete \acs{3D} shapes, and predict next best views if the recognition is uncertain. Maturana and Scherer \cite{Maturana2015} proposed a \acs{3D} \acs{CNN} for landing zone detection from \ac{LIDAR} data. In that work, they also introduced a volumetric representation for that data using a density occupancy grid. Later, they extended that work creating VoxNet \cite{Maturana2015a} a \acs{3D} \acs{CNN} architecture for real-time object classification  using volumetric occupancy grids to represent point clouds.

Other remarkable works are the multi-view system by Su \emph{et al.} \cite{Su2015}, the pano\-ra\-mic network by Shi \emph{et al.} \cite{Shi2015}, and the orientation-based voxel nets by Sedaghat \emph{et al.} \cite{Sedaghat2016}.

\newpage

\section{Model-based \acs{CNN}}
\label{sec:3dcnn_object_recognition:model_based}

In order to design, implement, and test our object recognition system, we are going to take a model-based approach which will be trained with full model clouds reconstructed from the extracted partial views. In that regard, we need two components: a proper \acs{2.5D} network architecture and data.

On the one hand, the network architecture of choice is shown in Figure \ref{fig:3dcnn:cnn_architecture}. This network is a slightly tuned version of the one introduced in PointNet \cite{Garcia-Garcia2016}, which was inspired in VoxNet and 3D ShapeNets. It has been implemented using Caffe (see Section \ref{sec:materials_and_methods:frameworks}). The model definition can be consulted in Appendix \ref{app:caffe_models}.

On the other hand, we need data to train the network and validate the results. For this approach we will merge the point clouds generated from the partial views -- as shown in Chapter \ref{cha:volumetric_representations} -- to create full model clouds. That model clouds will be transformed into the corresponding volumetric representations to feed the \acs{CNN} using a custom data layer implemented in Caffe.

\begin{figure}[!b]
	\centering
	\resizebox{\textwidth}{!}{
		\input{Figures/3dcnn/convnet_architecture.tikz}}
		\caption{\acs{2.5D} \acl{CNN} architecture used for the experiments. This network is an extension of the one presented in PointNet \cite{Garcia-Garcia2016}. It consists of a convolution layer -- $48$ filters, $3\times3$ filter with stride $1$ --, a \acs{ReLU} activation, another convolution layer -- $128$ filters, $5\times5$ filters with stride $1$ --, followed by a \acs{ReLU} activation, a pooling layer -- $2\times2$ max. pooling with stride $2$ --, a fully connected or inner product layer with $1024$ neurons and \acs{ReLU} activation, a dropout layer -- $0.5$ rate --, and an inner product layer with $10$ neurons as output. The network accepts \acs{3D} tensors as input.}
	\label{fig:3dcnn:cnn_architecture}
\end{figure}

\subsection{Experimentation}
\label{subsec:3dcnn_object_recognition:model_based:experimentation}

In order to assess the performance of the proposed model-based \acs{CNN} we carried out an extensive experimentation to determine the accuracy of the model and its robustness against occlusions and noise -- situations that often occur in real-world scenes. For that purpose we started using the normalized density grids (see Chapter \ref{cha:volumetric_representations}) since they offer a good balance between efficiency and representation. We also investigated the effect of both fixed and adaptive grids using different sizes. Further experimentation was performed to compare the normalized density grids with the binary ones.

The networks were trained for a maximum of $5000$ iterations -- weights were snapshotted every $100$ iterations so in the end we selected the best sets of them as if we were early stopping -- using Adadelta as optimizer with $\delta = 1\cdot10^{-8}$. The regularization term or weight decay in Caffe was set to $5\cdot10^{-3}$. A batch size of $32$ training samples was chosen. The Caffe solver file which we used to train the networks can be consulted in Appendix \ref{app:caffe_models}.

\newpage

Data was divided into training and validation sets as provided by Modelnet10. The training set was shuffled upon generation. The validation set was processed to add different levels of noise and occlusions as shown in Chapter \ref{cha:volumetric_representations} in order to test the robustness of the network.

\subsection{Results and Discussion}
\label{subsec:3dcnn_object_recognition:model_based:results}

Figure \ref{fig:3dcnn:experiments:25d_density} shows the accuracy results of the network for both grid types and increasing sizes. The peak accuracies for the fixed grids are $\approx0.75$, $\approx0.76$, and $\approx0.73$ for sizes $32$, $48$, and $64$ respectively. In the case of the adaptive one, the peak accuracies are $\approx0.77$, $\approx0.78$, and $\approx0.79$ for the sizes $32$, $48$, and $64$ respectively.

Taking those facts into account, we can extract two conclusions. First, the adaptive grid is able to achieve a slightly better peak accuracy in all cases; however, the fixed grid takes less iterations to reach accuracy values close to the peak in all cases. Second, there is no significant difference in using a bigger grid size of $64$ voxels instead of a smaller one of $32$.

The most important fact that can be observed in the aforementioned figures is that there is a considerable gap between training and validation accuracy in all situations. As we can observe, all networks reach maximum accuracy for the training set whilst the validation one hits a glass ceiling at approximately $0.80$. We hypothesize that the network suffers overfitting even when we thoroughly applied measures to avoid that. The most probable cause for that problem is the reduced number of training examples. In the case of ModelNet10 the training set consists of only $3991$ models. Considering the complexity of the \acs{CNN}, it is reasonable to think that the lack of a richer training set is causing overfitting.


\begin{figure}[!b]
	\centering
	\begin{subfigure}{0.49\textwidth}
		\input{Figures/3dcnn/experiments/25d_fixed_density.tikz}
		\caption{Fixed Grid}
		\label{fig:3dcnn:experiments:25d_fixed_density}
	\end{subfigure}
	\begin{subfigure}{0.49\textwidth}
		\input{Figures/3dcnn/experiments/25d_adaptive_density.tikz}
		\caption{Adaptive Grid}
		\label{fig:3dcnn:experiments:25d_adaptive_density}
	\end{subfigure}
	\caption{Evolution of training and validation accuracy of the model-based \acs{CNN} using both fixed \protect(\subref{fig:3dcnn:experiments:25d_fixed_density}) and adaptive \protect(\subref{fig:3dcnn:experiments:25d_adaptive_density}) normalized density grids. Different grid sizes ($32, 48, $ and $64$) were tested.}
	\label{fig:3dcnn:experiments:25d_density}
\end{figure}

\newpage

Another visualization format that would allow us to gain insight about the behavior of our network is the confusion matrix. Table \ref{table:confmatrix} shows a confusion matrix of the validation results achieved by the best performing network in our experiments. As we can observe, there is a lot of confusion between desks and tables (see Figure \ref{fig:3dcnn:experiments:confusion_desk_table}). In addition, night stands get usually misclassified as dressers (see Figure \ref{fig:3dcnn:experiments:confusion_nstand_dresser}). The other notable fact is that many sofas are misclassified as beds (see Figure \ref{fig:3dcnn:experiments:confusion_sofa_bed}). If we take a closer look at the confused classes it is reasonable to think that the network is not able to classify them properly because some samples are extremely similar.

\begin{table}[!ht]
	\centering
	\footnotesize
	\newcolumntype{C}{>{\centering\arraybackslash}p{2.35em}}%
	\pgfplotstabletypeset[
	column type=,
	begin table={\begin{tabularx}{0.8\textwidth}{CCCCCCCCCC}},
		end table={\end{tabularx}},
	color cells={min=1,max=100},
	col sep=comma,
	every head row/.style={after row=\toprule},
	every last row/.style={after row=\bottomrule},
    columns/Class/.style={string type},
	/pgfplots/colormap={whiteblue}{rgb255(0cm)=(255,255,255); rgb255(1cm)=(110,159,189)},]{
		Desk, Table, Nstand, Bed, Toil., Dresser, Bath., Sofa, Moni., Chair	
		52.0, 9.0, 1.0, 4.0, 0.0, 5.0, 1.0, 5.0, 0.0, 9.0
		25.0, 69.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 4.0
		1.0, 2.0, 60.0, 1.0, 4.0, 8.0, 0.0, 0.0, 2.0, 8.0
		4.0, 0.0, 0.0, 80.0, 0.0, 0.0, 3.0, 11.0, 1.0, 1.0
		1.0, 0.0, 3.0, 1.0, 84.0, 0.0, 1.0, 3.0, 2.0, 5.0
		3.0, 0.0, 14.0, 0.0, 0.0, 61.0, 0.0, 1.0, 6.0, 1.0
		0.0, 1.0, 0.0, 3.0, 0.0, 0.0, 34.0, 8.0, 3.0, 1.0
		1.0, 0.0, 1.0, 4.0, 1.0, 2.0, 0.0, 88.0, 1.0, 2.0
		1.0, 1.0, 1.0, 1.0, 0.0, 5.0, 1.0, 1.0, 87.0, 2.0
		1.0, 2.0, 1.0, 2.0, 1.0, 1.0, 0.0, 1.0, 1.0, 90.0
	}
	\caption{Confusion matrix of the validation results achieved by the best set of weights for the adaptive grid with a grid size of $64$ voxels. Darker cells indicate more predictions while lighter ones indicate less.}
	\label{table:confmatrix}
\end{table}

\begin{figure}[!b]
	\centering
	\includegraphics[width=0.38\textwidth]{3dcnn/confusions/desk} \hspace{2em}
	\includegraphics[width=0.43\textwidth]{3dcnn/confusions/table}
	\caption{A desk class sample together with a table class one.}
	\label{fig:3dcnn:experiments:confusion_desk_table}
\end{figure}

\begin{figure}[!b]
	\centering
	\includegraphics[width=0.38\textwidth]{3dcnn/confusions/nstand} \hspace{2em}
	\includegraphics[width=0.38\textwidth]{3dcnn/confusions/dresser}
	\caption{A night stand class sample together with a dresser one.}
	\label{fig:3dcnn:experiments:confusion_nstand_dresser}
\end{figure}

\newpage

\begin{figure}[!t]
	\centering
	\includegraphics[width=0.36\textwidth]{3dcnn/confusions/sofa}
	\includegraphics[width=0.36\textwidth]{3dcnn/confusions/bed}
	\caption{A sofa class sample together with a bed class one.}
	\label{fig:3dcnn:experiments:confusion_sofa_bed}
\end{figure}

\paragraph{Occlusion Resilience}

Concerning the robustness against occlusion, we took the best networks after training and tested them using the same validation sets as before but introducing occlusions in them (up to a $30\%$). Figure \ref{fig:3dcnn:experiments:25d_density_occlusion} shows the accuracy of both grid types with different sizes as the amount of occlusion in the validation model increases. As we can observe, occlusion has a significant and negative impact on the fixed grid -- bigger grid sizes are less affected -- going down from $\approx0.75$ accuracy to $0.40 - 0.50$ approximately in the worst and best case respectively when a $30\%$ of the model is occluded. On the contrary, the adaptive grid does not suffer that much -- it goes down from $\approx0.78$ to $\approx0.60$ in the worst case -- and there is no significant difference between grid sizes. In conclusion, the adaptive grid is considerably more robust to occlusion than the fixed one.

\begin{figure}[!b]
	\centering
	\begin{subfigure}{0.49\textwidth}
		\input{Figures/3dcnn/experiments/25d_fixed_density_occlusion.tikz}
		\caption{Fixed Grid}
		\label{fig:3dcnn:experiments:25d_fixed_density_occlusion}
	\end{subfigure}
	\begin{subfigure}{0.49\textwidth}
		\input{Figures/3dcnn/experiments/25d_adaptive_density_occlusion.tikz}
		\caption{Adaptive Grid}
		\label{fig:3dcnn:experiments:25d_adaptive_density_occlusion}
	\end{subfigure}
	\caption{Evolution of validation accuracy of the model-based \acs{CNN} using both fixed \protect(\subref{fig:3dcnn:experiments:25d_fixed_density_occlusion}) and adaptive \protect(\subref{fig:3dcnn:experiments:25d_adaptive_density_occlusion}) normalized density grids as the amount of occlusion in the validation models increases from $0\%$ to $30\%$. Three grid sizes were tested ($32, 48, $ and $64$).}
	\label{fig:3dcnn:experiments:25d_density_occlusion}
\end{figure}

\paragraph{Noise Robustness}

Regarding the resilience to noise, we also tested the best networks obtained from the aforementioned training process using validation sets with different levels of noise (ranging from $\sigma=1\cdot10^{-2}$ to $\sigma=1\cdot10^1$). Figure \ref{fig:3dcnn:experiments:25d_adaptive_density_noise} shows the results of those experiments. It can be observed that adding noise has a significant impact on the fixed grid, even small quantities, reducing the accuracy from $\approx0.75$ to $\approx0.60$, $\approx0.4$, and $\approx0.2$ for $\sigma=1\cdot10^{-1}, \sigma=1\cdot{10^0}$, and $\sigma=1\cdot10^1$ respectively. On the other hand, the adaptive one shows remarkable robustness against low levels of noise (up to $\sigma=1\cdot10^{-1}$), barely diminishing its accuracy.

In the end, both grids suffer huge penalties in accuracy when noise levels higher than $\sigma=1\cdot10^-1$ are introduced, being the adaptive one less affected. The grid size has little to no effect in both cases, only in the fixed grid bigger sizes are slightly more robust when intermediate to high levels of noise are introduced. In conclusion, the adaptive grid is significantly more resilient to low levels of noise, and slightly outperforms the fixed one when dealing with intermediate to high ones.

\begin{figure}[!t]
	\centering
	\begin{subfigure}{0.49\textwidth}
		\input{Figures/3dcnn/experiments/25d_fixed_density_noise.tikz}
		\caption{Fixed Grid}
		\label{fig:3dcnn:experiments:25d_fixed_density_noise}
	\end{subfigure}
	\begin{subfigure}{0.49\textwidth}
		\input{Figures/3dcnn/experiments/25d_adaptive_density_noise.tikz}
		\caption{Adaptive Grid}
		\label{fig:3dcnn:experiments:25d_adaptive_density_noise}
	\end{subfigure}
	\caption{Evolution of validation accuracy of the model-based \acs{CNN} using both fixed \protect(\subref{fig:3dcnn:experiments:25d_fixed_density_noise}) and adaptive \protect(\subref{fig:3dcnn:experiments:25d_adaptive_density_occlusion}) normalized density grids as the standard deviation of the Gaussian noise introduced in the $z$-axis of the views increases from $0.001$ to $10$. The common grid sizes were tested ($32, 48, $ and $64$).}
	\label{fig:3dcnn:experiments:25d_density_noise}
\end{figure}

\paragraph{Binary Occupancy}

After testing the performance of the normalized density grid, we also trained and assessed the accuracy of the binary one in the same scenarios. This test intended to show whether there is any gain in using representations which include more information about the shape -- at a small penalty to execution time.

For this experimentation we picked the best performer in the previous sections: the adaptive grid. We also discarded the intermediate size ($48$ voxels) since there was no significant difference between it and the others. Figure \ref{fig:3dcnn:experiments:25d_adaptive_binary} shows the accuracy results of the network trained using binary grids. As we can observe, there is no significant difference between grid sizes neither. However, using this representation we achieved a peak accuracy of approximately $0.85$, using $64$ voxels grids, which is better to some extent than the normalized density one shown in Figure \ref{fig:3dcnn:experiments:25d_density}.

Occlusion and noise tolerance (shown in Figures \ref{fig:3dcnn:experiments:25d_adaptive_binary_occlusion} and \ref{fig:3dcnn:experiments:25d_adaptive_binary_noise} respectively) is mostly similar to the robustness shown by the normalized density adaptive grid (see Figures \ref{fig:3dcnn:experiments:25d_adaptive_density_occlusion} and \ref{fig:3dcnn:experiments:25d_adaptive_density_noise}) except from a small offset caused by the higher accuracy of the binary grid network.

In conclusion, the less-is-better effect applies in this situation and turns out that the simplification introduced by the binary representation helps the network during the learning process. It is pending to check if this statement is still valid if the validation accuracy is not bounded by network overfitting.

\begin{figure}[!hbt]
	\centering
	\begin{subfigure}{\textwidth}
		\input{Figures/3dcnn/experiments/25d_adaptive_binary.tikz}
		\caption{Training}
		\label{fig:3dcnn:experiments:25d_adaptive_binary}
	\end{subfigure}
	\centering
	\par\bigskip
	\begin{subfigure}{0.49\textwidth}
		\input{Figures/3dcnn/experiments/25d_adaptive_binary_occlusion.tikz}
		\caption{Occlusion}
		\label{fig:3dcnn:experiments:25d_adaptive_binary_occlusion}
	\end{subfigure}
	\hfill
	\begin{subfigure}{0.49\textwidth}
		\input{Figures/3dcnn/experiments/25d_adaptive_binary_noise.tikz}
		\caption{Noise}
		\label{fig:3dcnn:experiments:25d_adaptive_binary_noise}
	\end{subfigure}
	\caption{Evolution of training and validation accuracy of the model-based \acs{CNN} using adaptive binary grids \protect(\subref{fig:3dcnn:experiments:25d_adaptive_binary}). Evolution of validation accuracy for the best network weights after training as the amount of occlusion in the validation set increases \protect(\subref{fig:3dcnn:experiments:25d_adaptive_binary_occlusion}) and different levels of noise are introduced \protect(\subref{fig:3dcnn:experiments:25d_adaptive_binary_noise}).}
	\label{fig:3dcnn:experiments:25d_binary}
\end{figure}

\clearpage

\paragraph{\acs{3D} Convolutions}

At last, we extended Caffe to support pure \acs{3D} convolutions and pooling operations. In fact, this implementation is generalized to $n$D convolutions and pooling layers, so the system could be easily extended to support sequences of \acs{3D} inputs for instance. We kept the same network architecture shown in Figure \ref{fig:3dcnn:cnn_architecture} but extended its convolution and pooling layers to three dimensions. The model definition can be consulted in Appendix \ref{app:caffe_models}.

We then trained the network using adaptive binary grids (since they were the ones with the best performance throughout the experimentation), and monitored the validation and training set accuracies during that process. The result is shown in Figure \ref{fig:3dcnn:experiments:3d_adaptive_binary}. As we can observe, the network does not perform as good as the \acs{2.5D} ones.

We trained the network for five times more iterations ($25000$) and the training accuracy slowly increased up to $\approx0.65$. However, the validation accuracy was stuck on $\approx0.4$ throughout the whole process.

In conclusion, porting the \acs{2.5D} network directly to \acs{3D} using the same datasets that produced good results in the former one does not achieve a comparable outcome in the latter. We hypothesize different causes for this problem.

On the one hand, the data representation might not be adequate for such fine-grained convolutions and bigger sizes or occupancy schemes might improve accuracy.

On the other hand, the complexity of the network skyrocketed after including that extra dimension to the convolutions, considerably increasing the number of weights and thus making the network harder to train with so few examples. This last assumption is backed up by the fact that the training accuracy keeps increasing while the validation one is stuck. This would eventually lead to a perfect fit to the training set but low accuracy on the validation examples. In other words, due to the excessive complexity of the network and the lack of training examples, the network suffers overfitting.

\begin{figure}[!hbt]
	\centering
	\input{Figures/3dcnn/experiments/3d_adaptive_density.tikz}
	\caption{Evolution of training and validation accuracy of the model-based \acs{CNN} with \acs{3D} convolutions, using adaptive binary grids with size $32\times32\times32$ voxels.}
	\label{fig:3dcnn:experiments:3d_adaptive_binary}
\end{figure}

\newpage

\begin{table}[!t]
	\resizebox{\textwidth}{!}{
	\begin{tabular}{ccccc}
		Grid Size & Fixed Density \acs{2.5D} & Adaptive Density \acs{2.5D} & Adaptive Binary \acs{2.5D} & Adaptive Binary \acs{3D}\\
		\hline
		$32\times32\times32$ & 0.75 & 0.77 & 0.80 & 0.43\\
		$48\times48\times48$ & 0.76 & 0.78 & N/A & N/A\\
		$64\times64\times64$ & 0.73 & 0.79 & 0.85 & N/A\\
\end{tabular}}
	\caption{Summary of the experimentation results.}
	\label{table:summary}
\end{table}

\section{Conclusion}
\label{sec:3dcnn_object_recognition:conclusion}

In this chapter we have discussed the differences between \acs{2D}, \acs{2.5D}, and \acs{3D} convolutions. We also reviewed state-of-the-art \acp{CNN} which make use of \acs{2.5D} and \acs{3D} convolutions. After that, we designed, implemented, and tested a \acs{2.5D} \acs{CNN} using the volumetric representations proposed in Chapter \ref{cha:volumetric_representations}. That network was trained and tested using fixed and adaptive grids with normalized density and binary occupancy measures. In addition, we tested the robustness of the network in adverse conditions by introducing occlusions and noise to the validation sets. After that, we extended the \acs{2.5D} network to \acs{3D} using the same data. A summary of the experimentation results is shown in Table \ref{table:summary}.

To sum up, we determined that the adaptive grid slightly outperforms the fixed one in normal conditions. The same happens with the grid size, obtaining marginally better results with bigger sizes. However, when it comes down to noise and occlusion robustness, the adaptive grid exceeds the accuracy of the fixed grid by a large margin for low levels of occlusion and noise, whilst for intermediate and high levels the impact on both grids is somewhat similar. The grid size had barely any effect on those tests. In other words, the adaptive grid is better than the fixed one and it is preferable to use a bigger grid size if the performance impact can be afforded.

It is important to remark that the binary occupancy measure performed better than the normalized density one, both using adaptive grids, while maintaining similar resilience against noise and occlusions. The best network trained with normalized density grids reached a peak accuracy of $\approx0.79$ while the best binary one achieved approximately a $0.85$ accuracy on the validation set.

Nevertheless, all networks exhibited a considerable amount of overfitting. This was due to the fact that the dataset has few training examples considering the complexity of the network and the classes are not completely distinguishable (for instance, many samples from the table class can be easily confused as desks). In this regard, the dataset must be augmented introducing noise, translations, rotations, and variations of the models to avoid overfitting and learn better those models that can be easily misclassified.

At last, we extended the network to support \acs{3D} convolutions and trained the same architecture as before using adaptive binary grids. The results were negative in the sense that the overfitting problem was accentuated due to the increased complexity of the network. By adding a third dimension to the convolutions, much more weights have to be learned, making the network prone to overfitting. In conclusion, despite the favorable properties of \acs{3D} convolutions, just extending a \acs{2.5D} network that works reasonably well, with a particular dataset, to perform \acs{3D} convolutions might not perform better as expected since training becomes harder.


