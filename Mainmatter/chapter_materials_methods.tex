\chapter{Materials and Methods}
\label{cha:materials_and_methods}

\begin{chapterabstract}
	In this second chapter we will describe the set of materials and methods employed in this work, in terms of software, data, and hardware. The chapter is organized as follows. Section \ref{sec:materials_and_methods:introduction} introduces the purpose of this chapter and puts the materials and methods in context. Section \ref{sec:materials_and_methods:framework} reviews a set of popular deep learning frameworks and justifies our chosen one. Section \ref{sec:materials_and_methods:datasets} analyzes existing object databases and selects one to be used for this work. At last, Section \ref{sec:materials_and_methods:hardware} describes the hardware setup that was assembled and configured to perform the experiments.
\end{chapterabstract}

\section{Introduction}
\label{sec:materials_and_methods:introduction}

In order to carry out this project, we need the support of many different resources. First, we need tools for expressing \acp{CNN}, which would allow us to design, implement, train, and test different layers, architectures, and configurations. It is possible to implement them from scratch, but leveraging to a framework eases and streamlines the development process.

Second, we need a source of data to generate training, test, and validation sets. That data will be adapted and transformed into a volumetric representation that will be eventually provided as input to the \acp{CNN}. The choice of a dataset is not trivial due to the special needs of deep architectures.

Third, we require computational resources to experiment with the implemented \acp{CNN} to measure their performance in terms of both execution time and accuracy. Again, because of the special requirements of deep neural networks, low-end computers are not a good option so high-end solutions with specialized hardware is a must for maintaining a competitive edge in this field.

In the following sections we will review each need in detail, focusing on picking a framework that best suits our needs, an adequate dataset for our purposes, and configuring a server to provide proper computational power.

\section{Frameworks}
\label{sec:materials_and_methods:framework}

Deep learning architectures have significantly influenced various application domains, surpassing by a large margin state-of-the-art methods for computer vision \cite{Krizhevsky2012, Russakovsky2015}, natural language processing \cite{Collobert2008, Collobert2011a, Glorot2011}, and speech recognition \cite{Deng2013, Deng2013a, Graves2013}. Due to that significant performance improvement achieved compared to traditional approaches, the popularity of deep learning methods has significantly increased over the last few years. Because of that, deep learning frameworks have also experienced a considerable growth in order to enable researchers to efficiently design, implement, and deploy these kind of architectures. Although this meteoric rise has hugely benefited the deep learning community, it also leads researchers to a certain confusion and difficulties to decide which framework to use. The fact is that different frameworks aim to optimize distinct aspects of the development process of deep learning architecture, i.e., choosing one framework over the others is mainly a matter of adequateness to the task at hand.

\begin{table}[!t]
	\centering
	\resizebox{\linewidth}{!}{
	\begin{tabular}{cccccc}
		Framework & License & Core & Interface & \acs{OpenMP} & \acs{CUDA}\\
		\hline
		Torch\cite{Collobert2011} & BSD 3-Clause & C, Lua & Torch, C, \CC{}/\acs{OpenCL} & Yes & Third Party\\
		Theano\cite{Bergstra2011} & BSD 3-Clause & Python & Python & Yes & Yes\\
		Caffe\cite{Jia2014} & BSD 2-Clause & \CC{}, Python & \CC{}, \acs{CLI}, Python, MATLAB & No & Yes\\
		\acs{CNTK}\cite{Agarwal2014} & MIT & \CC{} & \acs{CLI} & No & Yes\\
		TensorFlow\cite{Abadi2015} & Apache 2.0 & \CC{}, Python & C/\CC{}, Python & No & Yes\\
	\end{tabular}}
	\caption[Comparison of the most popular deep learning frameworks]{Comparison of the most popular deep learning frameworks, taking into account high-level features such as the source code license, the programming language used to write the core of the framework, the available interfaces to train, test, and deploy the models, and whether or not they support \ac{OpenMP} and \ac{CUDA}.}
	\label{tab:deep-learning-frameworks}
\end{table}

The list of available frameworks is vast and it is growing constantly as new frameworks are developed either to fill the gaps left by other's weaknesses or to provide new approaches and tools for researches to ease the development of deep architectures. Arguably, the most popular frameworks nowadays are Theano, Torch, Caffe, \acf{CNTK}, and TensorFlow. The list is not limited to those frameworks and also includes other less known ones such as DeepLearning4J, Neon, PyLearn, MXNet, ConvNet, CUDA-ConvNet, Deepmat, and more too numerous to mention. This section presents an overview of the five aforementioned most popular frameworks, as well as a brief comparison and discussion about their features to determine the best one for our purposes. In this regard, Table \ref{tab:deep-learning-frameworks} shows a quick comparison of the five discussed frameworks, taking into account certain high-level features. This brief comparison serves as a proper starting point to describe each one of the frameworks in depth throughout the next sections.

\subsection{Torch}

\emph{Torch}\cite{Collobert2011} is a scientific computing framework mainly maintained by researchers at \ac{FAIR} lab, Google DeepMind, Twitter, and also by a large list of contributors on \emph{GitHub}\footnote{\url{http://github.com/torch/torch7}}. It is a BSD-licensed C/Lua library that runs on Lua \ac{JIT} compiler with wide support for machine learning algorithms. Its goal is to have maximum flexibility and speed building scientific algorithms while making the process extremely simple thanks to an easy and fast scripting language, LuaJIT, and an underlying C/\ac{CUDA} implementation. One of its main highlights is its large ecosystem of community-driven packages and documentation.

The most important core features are easiness (it uses a simple scripting language as LuaJIT as an interface to the C core routines), efficiency (the underlying C/\ac{CUDA} implementation provides a strong backend for both CPU and \ac{GPU} computations with NVIDIA \ac{cuDNN}\cite{Chetlur2014}), optimized routines (for linear algebra, numeric optimization, and machine learning), and fast embedding with ports to iOS, Android and \ac{FPGA} backends.

\subsection{Theano}

\emph{Theano}\cite{Bergstra2010, Bergstra2011, Bastien2012} is a mathematical compiler for Python developed by a group of researches from the University of Montreal \ac{CSO} department, and maintained by them together with the contributions of a number users on \emph{GitHub}\footnote{\url{https://github.com/Theano/Theano}}. It is a BSD-licensed framework in the Python programming language  which can be used to define mathematical functions in a symbolic way, derive gradient expressions, and compile those expressions into executable functions for efficient performance in both CPU and \ac{GPU} platforms. It was conceived as a general mathematical tool, but its main goal was to facilitate research in deep learning.

The main features of Theano are the following ones: powerful tools for manipulating and optimizing symbolic mathematical expressions, fast to write and execute thanks to its dependency on NumPy\cite{VanDerWalt2011} and SciPy\cite{Jones2014}, \ac{CUDA} code generators for fast GPU execution of compiled mathematical expressions, and code stability and community support due to its short stable release cycle and exhaustive testing.

\subsection{Caffe}

\emph{Caffe}\cite{Jia2014} is a deep learning framework developed and maintained by the \ac{BVLC} and an active community of contributors on \emph{GitHub}\footnote{\url{http://github.com/BVLC/caffe}}. This BSD-licensed \CC{} library provides means to create, train, and deploy general-purpose \acp{CNN} and other deep models efficiently, mainly thanks to its drop-in integration of NVIDIA \ac{cuDNN}\cite{Chetlur2014} to take advantage of GPU acceleration.

The highlights of this framework are modularity (allowing easy extension by defining new data formats, layers, and functions), separation of representation and implementation (model definitions are written as configuration files in \emph{Protocol Buffer} language), test coverage (all modules have tests which have to be passed to be accepted into the project), Python/MATLAB bindings (for rapid prototyping, besides the \ac{CLI} and \CC{} \acs{API}), and pre-trained reference models.

\subsection{\acl{CNTK}}

The \ac{CNTK} \cite{Agarwal2014} is a general purpose machine learning framework developed by Microsoft Research and recently open-sourced under the MIT license on \emph{GitHub}\footnote{\url{https://github.com/Microsoft/CNK}}. Its main contribution is providing a unified framework for describing learning systems as \ac{CN}, and the means for training and evaluating that series of computational steps. Despite the fact that it was created as a general toolkit, it mainly focuses on deep neural networks, so it can be redefined as a deep learning tool that balances efficiency, performance, and flexibility. The core is written in \CC{} and the \ac{CN} models can be described in both \CC{} or \ac{NDL}/\ac{MEL}.

Thanks to the directed graph representation of the computation steps, it allows to easily recreate common models and extend them or create new ones from scratch by combining simple functions in arbitrary ways. It features a significantly modularized architecture, \ac{SGD} for training, auto-differentiation to obtain the gradient derivatives, and parallelization focused on \ac{GPU} clusters across multiple nodes with \ac{CUDA}-capable devices.

\subsection{TensorFlow}

TensorFlow\cite{Abadi2015} is a library for numerical computation using data flow graphs originally developed by researchers and engineers working on the Google Brain team for the purposes of conducting machine learning and deep neural networks research, currently an active community of \emph{GitHub} users contribute to the source code as well using its repository \footnote{\url{https://github.com/tensorflow/tensorflow}}. Its core is written in \CC{} and the whole framework is being open sourced under the Apache 2.0 license. The main goal of this project is to provide an interface, in both Python and \CC{}, for expressing artificial intelligence or machine learning algorithms, together with an efficient implementation of those algorithms which are flexible and scalable, i.e., the computations can be executed on a wide variety of devices with minimal or no changes at all.

This library features deep flexibility (every computation that can be expressed as a data flow graph can be implemented using TensorFlow), portability and scalability (training, testing, and deployment run on both CPUs and \acp{GPU} and they scale from single nodes to large-scale systems, besides it allows users to freely assign compute elements of the graph to different devices), research and production (deploying experimental systems is an easy task and requires minimal changes), and auto-differentiation (the derivatives needed for the data flow graph when expressing gradient based machine learning algorithms are computed automatically).

\subsection{Other Frameworks}

Other less known but still important frameworks that are currently being use by the research community are (among others) Keras \footnote{\url{https://github.com/fchollet/keras}}, DeepLearning4j \footnote{\url{https://github.com/deeplearning4j/deeplearning4j}}, Marvin \footnote{\url{https://github.com/PrincetonVision/marvin}}, and MatConvNet \cite{Vedaldi2015} \footnote{\url{https://github.com/vlfeat/matconvnet}}.

\subsection{Picking a Framework}

Picking the right framework depends on many factors, e.g., speed, extensibility, main language, the community, etc. In our case, our main concern is speed since the system is intended to be eventually deployed in an embedded platform for real-time recognition. Related to this point, it is important for us the compatibility of the framework to export and load models in embedded boards for classification. Another major concern is extensibility to create our own data layers and extend convolutions from \acs{2D} to \acs{3D}. And last, but not least, having an active community is a must for choosing a framework in order to effectively deal with problems and improve the current code base.

Taking all of this into account, we picked Caffe as our framework of choice due to its long standing community, extensibility, speed -- thanks to \CC{} and \acs{CUDA} --, and its well-tested deployment in embedded platforms. The framework was extended this framework to support \acs{3D} convolutions and pooling. In addition, we also added a data layer to load our volumetric representations that will be discussed in Chapter \ref{cha:volumetric_representations}.

\newpage

\section{Datasets}
\label{sec:materials_and_methods:datasets}

Deep neural network architectures are usually composed by many layers which in turn mean many weights to be learned. Because of that, there is a strong need of large-scale datasets to train those networks in order to avoid overfitting the model to the input data. Nowadays, large-scale databases of real-world \acs{3D} objects are scarce, some of them do not have that high number of objects \cite{Lai2011}\cite{Singh2014}\cite{Li2014}, or were incomplete by the time this work was performed \cite{Choi2016}. A possible workaround to this problem consists of using \ac{CAD} model databases -- which are virtually unlimited -- and processing those models to simulate real-world data.

The \emph{Princeton ModelNet} project is one of the most popular large-scale \acs{3D} object dataset. Its goal, as their authors state, is to provide researchers with a comprehensive clean collection of \acs{3D} \ac{CAD} models for objects, which were obtained via online search engines. Employees from the \ac{AMT} service were hired to classify over \num{150000} models into \num{662} different categories.

At the moment, there are two versions of this dataset publicly available for download\footnote{\url{http://modelnet.cs.princeton.edu/}}: \emph{ModelNet-10} and \emph{ModelNet-40}. Those are subsets of the original dataset which only provide the \num{10} and \num{40} most popular object categories respectively. These subsets are specially clean versions of the complete dataset.

On the one hand, ModelNet-10 is composed of a collection of over 5000 models classified into 10 categories and divided into training and test splits. In addition, the orientation of all \ac{CAD} models of the dataset was manually aligned. On the other hand, ModelNet-40 features over \num{9800} models classified into \num{40} categories, also including training and test sets. However, the orientations of its models are not aligned as they are in ModelNet-10. Figure \ref{fig:modelnet-distribution} and Table \ref{tab:modelnet-distribution} show the model distribution per each class of both subsets taking into account the training and test splits. Figure \ref{fig:modelnet-models} shows some model examples from ModelNet-10.

\begin{figure}[!b]
	\centering
	\input{Figures/materials_methods/modelnet-distribution.tikz}
	\caption[Model distribution for ModelNet-10 and ModelNet-40]{Model distribution per object class or category for both ModelNet-10 and ModelNet-40 training and test splits.}
	\label{fig:modelnet-distribution}
\end{figure}

\clearpage

\begin{figure}[!hbt]
	\centering
	\includegraphics[width=0.15\textwidth]{Figures/materials_methods/modelnet/desk_0}\hfill
	\includegraphics[width=0.15\textwidth]{Figures/materials_methods/modelnet/desk_1}\hfill
	\includegraphics[width=0.15\textwidth]{Figures/materials_methods/modelnet/desk_2}\hfill
	\includegraphics[width=0.15\textwidth]{Figures/materials_methods/modelnet/desk_3}\hfill
	\includegraphics[width=0.15\textwidth]{Figures/materials_methods/modelnet/desk_4}\hfill

	\includegraphics[width=0.15\textwidth]{Figures/materials_methods/modelnet/table_0}\hfill
	\includegraphics[width=0.15\textwidth]{Figures/materials_methods/modelnet/table_1}\hfill
	\includegraphics[width=0.15\textwidth]{Figures/materials_methods/modelnet/table_2}\hfill
	\includegraphics[width=0.15\textwidth]{Figures/materials_methods/modelnet/table_3}\hfill
	\includegraphics[width=0.15\textwidth]{Figures/materials_methods/modelnet/table_4}\hfill

	\includegraphics[width=0.15\textwidth]{Figures/materials_methods/modelnet/nstand_0}\hfill
	\includegraphics[width=0.15\textwidth]{Figures/materials_methods/modelnet/nstand_1}\hfill
	\includegraphics[width=0.15\textwidth]{Figures/materials_methods/modelnet/nstand_2}\hfill
	\includegraphics[width=0.15\textwidth]{Figures/materials_methods/modelnet/nstand_3}\hfill
	\includegraphics[width=0.15\textwidth]{Figures/materials_methods/modelnet/nstand_4}\hfill

	\includegraphics[width=0.15\textwidth]{Figures/materials_methods/modelnet/bed_0}\hfill
	\includegraphics[width=0.15\textwidth]{Figures/materials_methods/modelnet/bed_1}\hfill
	\includegraphics[width=0.15\textwidth]{Figures/materials_methods/modelnet/bed_2}\hfill
	\includegraphics[width=0.15\textwidth]{Figures/materials_methods/modelnet/bed_3}\hfill
	\includegraphics[width=0.15\textwidth]{Figures/materials_methods/modelnet/bed_4}\hfill

	\includegraphics[width=0.15\textwidth]{Figures/materials_methods/modelnet/toilet_0}\hfill
	\includegraphics[width=0.15\textwidth]{Figures/materials_methods/modelnet/toilet_1}\hfill
	\includegraphics[width=0.15\textwidth]{Figures/materials_methods/modelnet/toilet_2}\hfill
	\includegraphics[width=0.15\textwidth]{Figures/materials_methods/modelnet/toilet_3}\hfill
	\includegraphics[width=0.15\textwidth]{Figures/materials_methods/modelnet/toilet_4}\hfill

	\includegraphics[width=0.15\textwidth]{Figures/materials_methods/modelnet/dresser_0}\hfill
	\includegraphics[width=0.15\textwidth]{Figures/materials_methods/modelnet/dresser_1}\hfill
	\includegraphics[width=0.15\textwidth]{Figures/materials_methods/modelnet/dresser_2}\hfill
	\includegraphics[width=0.15\textwidth]{Figures/materials_methods/modelnet/dresser_3}\hfill
	\includegraphics[width=0.15\textwidth]{Figures/materials_methods/modelnet/dresser_4}\hfill
	
	\includegraphics[width=0.15\textwidth]{Figures/materials_methods/modelnet/bathtub_0}\hfill
	\includegraphics[width=0.15\textwidth]{Figures/materials_methods/modelnet/bathtub_1}\hfill
	\includegraphics[width=0.15\textwidth]{Figures/materials_methods/modelnet/bathtub_2}\hfill
	\includegraphics[width=0.15\textwidth]{Figures/materials_methods/modelnet/bathtub_3}\hfill
	\includegraphics[width=0.15\textwidth]{Figures/materials_methods/modelnet/bathtub_4}\hfill

	\includegraphics[width=0.15\textwidth]{Figures/materials_methods/modelnet/sofa_0}\hfill
	\includegraphics[width=0.15\textwidth]{Figures/materials_methods/modelnet/sofa_1}\hfill
	\includegraphics[width=0.15\textwidth]{Figures/materials_methods/modelnet/sofa_2}\hfill
	\includegraphics[width=0.15\textwidth]{Figures/materials_methods/modelnet/sofa_3}\hfill
	\includegraphics[width=0.15\textwidth]{Figures/materials_methods/modelnet/sofa_4}\hfill

	\includegraphics[width=0.15\textwidth]{Figures/materials_methods/modelnet/monitor_0}\hfill
	\includegraphics[width=0.15\textwidth]{Figures/materials_methods/modelnet/monitor_1}\hfill
	\includegraphics[width=0.15\textwidth]{Figures/materials_methods/modelnet/monitor_2}\hfill
	\includegraphics[width=0.15\textwidth]{Figures/materials_methods/modelnet/monitor_3}\hfill
	\includegraphics[width=0.15\textwidth]{Figures/materials_methods/modelnet/monitor_4}\hfill

	\includegraphics[width=0.15\textwidth]{Figures/materials_methods/modelnet/chair_0}\hfill
	\includegraphics[width=0.15\textwidth]{Figures/materials_methods/modelnet/chair_1}\hfill
	\includegraphics[width=0.15\textwidth]{Figures/materials_methods/modelnet/chair_2}\hfill
	\includegraphics[width=0.15\textwidth]{Figures/materials_methods/modelnet/chair_3}\hfill
	\includegraphics[width=0.15\textwidth]{Figures/materials_methods/modelnet/chair_4}\hfill
	\caption{ModelNet10 samples.}
	\label{fig:modelnet-models}
\end{figure}

\clearpage

\begin{table}[!t]
	\centering
	\input{Tables/materials_methods/modelnet-distribution.tikz}
	%\caption[Model distribution for ModelNet-10 and ModelNet-40]{Model distribution per object class or category for both ModelNet-10 and ModelNet-40 training and test splits.}
	\caption{Model distribution per object class or category for both ModelNet-10 and ModelNet-40 training and test splits.}
	\label{tab:modelnet-distribution}
\end{table}

For this work, we will use of the ModelNet-10 subset, which contains a reasonable amount of models for both training and validation, mainly because this dataset was completely cleaned and the orientation of the models were manually aligned.

\newpage

\section{Hardware}
\label{sec:materials_and_methods:hardware}

Deep learning algorithms take tremendous computational resources to efficiently process huge amounts of data. To that end, it becomes mandatory to experiment in a powerful and energy-efficient computing solution. The \emph{Asimov} server was assembled and configured for that purpose. It was built with the NVIDIA Digits DevBox \cite{DevBox} as inspiration, so most of its components were chosen based on the DevBox's ones.

The full hardware configuration is shown in Table \ref{table:asimov}. The main features of the server are the three NVIDIA \acp{GPU} which were targeted at different goals. The Titan X will be devoted to deep learning, whilst a Tesla K40 was also installed for scientific computation purposes thanks to its exceptional performance with double precision floating point numbers. In addition, a less powerful GT730 was included for visualization and offloading the Titan X from that burden.

Regarding the software information, the server runs Ubuntu $16.04$ LTS with Linux kernel $4.4.0-21-$generic for x86\_64 architecture. The \acp{GPU} are running on NVIDIA driver version $361.42$ and \acs{CUDA} $7.5$.

Other relevant software includes Caffe RC$3$, \ac{PCL} $1.8$ (master branch \texttt{9260fa2}), Vtk $5.6$, and Boost $1.58.0.1$. All libraries and tools were compiled using \ac{GCC} $5.3.1$ and the CMake $3.5.1$ environment with release settings for maximum optimization.

\begin{table}[!b]
	\centering
	\begin{tabular}{ll}
		\hline
		\multicolumn{2}{c}{Asimov}\\
		\hline
		Motherboard & Asus X99-A \footnotemark[11]\\
								& ~ Intel X99 Chipset\\
								& ~ $4\times$ PCIe $3.0/2.0 \times16 (\times16, \times16/\times16, \times16/\times16/\times8)$\\
		CPU & Intel(R) Core(TM) i7-5820K CPU @ 3.30GHz \footnotemark[12]\\
						& ~ $3.3$ GHz ($3.6$ GHz Turbo Boost)\\
						& ~ $6$ cores ($12$ threads)\\
						& ~ $140$ W TDP\\
		GPU (visualization) & NVIDIA GeForce GT730 \footnotemark[13]\\
												& ~ $96$ \acs{CUDA} cores\\
												& ~ $1024$ MiB of DDR3 Video Memory\\
												& ~ PCIe 2.0\\
												& ~ $49$ W TDP\\
		GPU (deep learning) & NVIDIA GeForce Titan X \footnotemark[14]\\
												& ~ $3072$ \acs{CUDA} cores\\
												& ~ $12$ GiB of GDDR5 Video Memory\\
												& ~ PCIe 3.0\\
												& ~ $250$ W TDP\\
		GPU (compute) & NVIDIA Tesla K40c \footnotemark[15]\\
									& ~ $2880$ \acs{CUDA} cores\\
									& ~ $12$ GiB of GDDR5 Video Memory\\
									& ~ PCIe 3.0\\
									& ~ $235$ W TDP\\
		RAM & $4\times8$ GiB Kingston Hyper X DDR4 $2666$ MHz CL$13$\\
		Storage (Data) & (\acs{RAID}1) Seagate Barracuda 7200rpm 3TiB SATA III \acs{HDD} \footnotemark[16]\\
		Storage (OS) & Samsung 850 EVO 500GiB SATA III \acs{SSD} \footnotemark[17]\\
		\hline
	\end{tabular}
	\caption{Hardware specifications of Asimov.}
	\label{table:asimov}
\end{table}

\footnotetext[11]{\url{https://www.asus.com/Motherboards/X99A/specifications/}}
\footnotetext[12]{\url{http://ark.intel.com/products/82932/Intel-Core-i7-5820K-Processor-15M-Cache-up-to-3_60-GHz}}
\footnotetext[13]{\url{http://www.geforce.com/hardware/desktop-gpus/geforce-gt-730/specifications}}
\footnotetext[14]{\url{http://www.geforce.com/hardware/desktop-gpus/geforce-gtx-titan-x}}
\footnotetext[15]{\url{http://www.nvidia.es/content/PDF/kepler/Tesla-K40-PCIe-Passive-Board-Spec-BD-06902-001_v05.pdf}}
\footnotetext[16]{\url{http://www.seagate.com/es/es/internal-hard-drives/desktop-hard-drives/desktop-hdd/?sku=ST3000DM001}}
\footnotetext[17]{\url{http://www.samsung.com/us/computer/memory-storage/MZ-75E500B/AM}}

\newpage

In addition, the server was configured for remote access using \ac{SSH}. The installed version is OpenSSH $7.2$p$2$ with OpenSSL $1.0.2$. Authentication based on public/private key pairs was configured so only authorized users can access the server through an \ac{SSH} gateway with the possibility to forward X11 through the \ac{SSH} connection for visualization purposes. Figure \ref{fig:asimov_ssh} shows an \ac{SSH} connection to Asimov.

At last, a mirrored \ac{RAID}1 setup was configured with both \acp{HDD} for optimized reading and redundancy to tolerate errors on a data partition to store all the needed datasets, intermediate results, and models. The \ac{SSD} was reserved for the operating system, swap, and caching.

\begin{figure}[!t]
	\centering
	\includegraphics[width=\textwidth]{materials_methods/ssh}
	\caption{Asimov's \acs{SSH} banner message and welcome screen with server info.}
	\label{fig:asimov_ssh}
\end{figure}
