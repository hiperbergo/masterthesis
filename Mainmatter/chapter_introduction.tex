\chapter{Introduction}
\label{cha:introduction}

\begin{chapterabstract}
	This first chapter introduces the main topic of this work. It is organized as follows. Section \ref{sec:introduction:overview} sets up the framework for the activities performed during this thesis. Section \ref{sec:introduction:motivation} introduces the motivation of this work. Section \ref{sec:introduction:related_works} elaborates a state of the art of object recognition systems and the evolution of the problem during the last years. Section \ref{sec:introduction:proposal_goals} lays down the proposal developed in this work and  presents the main and specific goals of this project. In the end, Section \ref{sec:introduction:outline} details the structure of this document.
\end{chapterabstract}

\section{Overview}
\label{sec:introduction:overview}

In this Master's Thesis, we have performed a theoretical and practical research focused on the problem of \acs{3D} object class recognition. The main goal is the proposal, design, implementation, and validation of an efficient and accurate \acs{3D} object recognition system. We leveraged deep learning techniques to solve this problem, specifically \aclp{CNN}. This work comprises the study of the theoretical foundations of \acp{CNN}, the analysis of volumetric representations for \acl{3D} data, and the implementation of a \acs{3D} \ac{CNN}.

\section{Motivation}
\label{sec:introduction:motivation}

This document presents the work carried out to prove the knowledge acquired during the \emph{Master's Degree in Automation and Robotics}, taken between the years 2015 and 2016 at the \emph{University of Alicante}. This work follows up the line started with my Bachelor's Thesis \cite{Garcia-Garcia2015}\cite{Garcia-Garcia2016a}. The motivation for this work is twofold.

On the one hand, part of this work was carried out during the research collaboration period with the \emph{Industrial Informatics and Computer Networks} research group of the \emph{Department of Computer Technology} at the \emph{University of Alicante}. This collaboration was supervised by Prof. José García-Rodríguez and funded by the grant "Ayudas para estudios de master oficial e iniciación a la investigación" from the "Desarrollo e innovación para el fomento de la I+D+i en la Universidad de Alicante" programme. The purpose of this collaboration is to introduce students into a certain line of research, in our case we were mainly focused on deep learning applied to computer vision.

On the other hand, this work has been performed under the frame of the \emph{SIRMAVED: Development of a comprehensive robotic system for monitoring and interaction for people with acquired brain damage and dependent people} \cite{Cazorla2015} national project (identifier code DPI2013-40534-R). Within this project, an object recognition system is needed to identify instances in the environment before handling or grasping them. This project is funded by the Ministerio de Economía y Competitividad (MEC) of Spain with professors José García-Rodríguez and Miguel Ángel Cazorla-Quevedo from the University of Alicante as main researchers.

\section{Related Works}
\label{sec:introduction:related_works}

In the context of computer vision, object classification and detection are two of the most challenging tasks required to achieve scene understanding. The former focuses on predicting the existence of objects within a scene, whereas the latter aims to localize those objects. Figure \ref{fig:object_classification_detection} shows a visual comparison of both tasks.

\begin{figure}[!b]
	\centering
	\begin{subfigure}{0.49\textwidth}
		\centering
		\input{Figures/related-works/object_classification.tikz}
		\caption{Object classification.}
		\label{subfig:object_classification}
	\end{subfigure}
	\hfill
	\begin{subfigure}{0.49\textwidth}
		\centering
		\input{Figures/related-works/object_detection.tikz}
		\caption{Object detection.}
		\label{subfig:object_detection}
	\end{subfigure}
	%\caption[Object classification and detection examples]{The object classification task focuses on providing a list of objects that are present in the provided image with an associated probability, as shown in Subfigure (\subref{subfig:object_classification}). On the other hand, the object detection task exhibits more complexity since it provides a bounding-box or even an estimate pose for each object in the image, Subfigure (\subref{subfig:object_detection}) shows an example of detection.}
	\caption{The object classification task focuses on providing a list of objects that are present in the provided image with an associated probability, as shown in Subfigure (\subref{subfig:object_classification}). On the other hand, the object detection task exhibits more complexity since it provides a bounding-box or even an estimate pose for each object in the image, Subfigure (\subref{subfig:object_detection}) shows an example of detection.}
	\label{fig:object_classification_detection}
\end{figure}

Since the very beginning of the computer vision research field, a considerable amount of effort has been directed towards achieving robust object recognition systems that are able to semantically classify a scene and detect objects within it, also providing their estimated poses \cite{Andreopoulos2013}. This is due to the fact that semantic object recognition is a key capability required by cognitive robots to being able to operate autonomously in unstructured, real-world environments.

\subsection{Feature-based Object Recognition}
\label{subsec:introduction:feature-based_object_recognition}

Over the past few years, intensive research has been done on feature-based object recognition methods. This approach relies on extracting features, i.e., pieces of information which describe simple but significant properties of the objects within the scene. Those features are encoded into \emph{descriptors} such as \ac{SIFT}\cite{Lowe2004}, \ac{SURF}\cite{Bay2006}, \ac{BRIEF}\cite{Calonder2010}, \ac{BRISK}\cite{Leutenegger2011}, \ac{ORB}\cite{Rublee2011}, or \ac{FREAK}\cite{Alahi2012} to name a few. After extracting those descriptors, machine learning techniques are applied to train a system with them so that it becomes able to classify features extracted from unknown scenes. Based on the used types of features, these methods can be divided into two categories: global or local feature-based methods. Global ones are characterized by dealing with the object as a whole; they define a set of features which completely encompass the object and describe it effectively. On the other hand, local methods describe local patches of the object, those regions are located around highly distinctive spots of the object named \emph{keypoints}. 

Real-world scenes tend to be unstructured environments. This implies that object recognition systems must not be affected by clutter or partial occlusions. In addition, they should be invariant to illumination, transforms, and object variations. Those are the main reasons why local surface feature-based methods have been popular and successful during the last years -- since they do not need the whole object to describe it properly, they are able to cope with cluttered environments and occlusions \cite{Lowe1999}. 

\subsection{Usage of 3D Data}
\label{subsec:introduction:usage_of_3d_data}

Traditionally, those object recognition systems made use of \acs{2D} images with intensity or color information, i.e., \ac{RGB} images. However, technological advances made during the last years have caused a huge increase in the usage of \acs{3D} information. The field of computer vision in general, and object recognition research in particular, have been slowly but surely moving towards including this richer information into their algorithms.

Nowadays, the use of \acs{3D} information for this task is in a state of continuous evolution, still far behind, in terms of maturity, from the systems that make use of \acs{2D} images. Nevertheless, the use of \acs{2D} information exhibits a handful of problems which hinder the development of robust object recognition systems. Oppositely, the use of range images or point clouds, which provide \acs{2.5D} or \acs{3D} information respectively, presents many significant benefits over traditional \acs{2D}-based systems. Some of the main advantages are the following ones \cite{Guo2014}: (1) they provide geometrical information thus removing surface ambiguities, (2) many of the features that can be extracted are not affected by illumination or even scale changes, (3) pose estimation is more accurate due to the increased amount of surface information. Therefore, the use of \acs{3D} data has become a solid choice to overcome the inherent hurdles of traditional \acs{2D} methods. 

\begin{figure}[!b]
	\input{Figures/related-works/terms_statistics.tikz}
	%\caption[Evolution of \acs{2D}, \acs{3D}, and Deep Learning terms usage in computer vision]{Evolution of the number of academic documents containing the terms 2D, 3D, and Deep Learning together with \emph{Computer Vision}. Search terms statistics obtained from \href{http://www.scopus.com/}{scopus.com}.}
	\caption{Evolution of the number of academic documents containing the terms 2D, 3D, and Deep Learning together with \emph{Computer Vision}. Search terms statistics obtained from \href{http://www.scopus.com/}{scopus.com}.}
	\label{fig:terms_statistics}
\end{figure}

However, despite all the advantageous assets of \acs{3D} data, researchers had to overcome certain difficulties or drawbacks. On the one hand, sensors capable of providing \acs{3D} were expensive, limited, and performed poorly in many cases. The advent of low-cost \acs{3D} acquisition systems, e.g., Microsoft Kinect, enabled a widespread adoption of these kind of sensors thanks to their accessibility and affordability. On the other hand, \acs{3D} object recognition systems are computationally intensive due to the increased dimensionality. In this regard, advances in computing devices like \acp{GPU} provided enough computational horsepower to run those algorithms in an efficient manner. In addition, the availability of low-power \acs{GPU} computing devices like the NVIDIA Jetson TK1 has supposed a significant step towards deploying robust and powerful object recognition systems in mobile robotic platforms.

The combination of those three factors (the advantages of \acs{3D} data, low-cost sensors, and parallel computing devices) transformed the field of computer vision in general, and object recognition in particular. As we can see in Figure \ref{fig:terms_statistics}, there has been a significant dominance of \acs{3D} over \acs{2D} research in computer vision since the year 2000.

Therefore, creating a robust \acs{3D} object recognition system, which is also able to work in real time, became one of the main goals towards for computer vision researchers \cite{Ponce2004}. There exist many reviews about \acs{3D} object recognition in the literature, including the seminal works of Besl and Rain \cite{Besl1985}, Brady et al. \cite{Brady1989}, Arman et al. \cite{Arman1993}, Campbell and Flynn \cite{Campbell2001}, and Mamic and Bennamoun \cite{Mamic2002}. All of them perform a general review of the \acs{3D} object recognition problem with varying levels of detail and different points of view. The work of Guo et al. \cite{Guo2014} is characterized by its comprehensive analysis of different local surface feature-based \acs{3D} object recognition methods which were published between the years 1992 and 2013. In that review, they explain the main advantages and drawbacks of each one of them. They also provide an in-depth survey of various techniques used in each phase of a \acs{3D} object recognition pipeline, from the keypoint extraction stage to the surface matching one, including the extraction of local surface descriptors. The review is specially remarkable due to its freshness and level of detail. It is important to remark that all the described methods make use of carefully designed feature descriptors by experts in the field.

\subsection{Learning Features Automatically by Means of Deep Learning}
\label{subsec:introduction:learning_features_automatically_by_means_of_deep_learning}

From the earliest days of computer vision, the aim of researchers has been to replace hand-crafted feature descriptors, which require domain expertise and engineering skills, with multilayer networks able to learn them automatically by using a general-purpose training algorithm \cite{Lecun2015}. The solution for this problem was discovered during the 1970s and 1980s by different research groups independently \cite{Werbos1974}\cite{LeCun1985}\cite{Rumelhart1988}. This gave birth to a whole new branch of machine learning named deep learning.

\begin{figure}[!b]
	\centering
	\includegraphics[width=0.7\textwidth]{Figures/related-works/weights}
	%\caption[Filters learned by the network proposed by Krizhevsky et al.]{Filters learned by the network proposed by Krizhevsky et al. \cite{Krizhevsky2012}. Each of the 96 filters shown is of size $11\times11\times3$. The filters have clearly learned to detect edges of various orientations and they resemble Gabor filters. Image analysis using that kind of filters is thought to be similar to perception in the human visual system \cite{Marvcelja1980}.}
	\caption{Filters learned by the network proposed by Krizhevsky et al. \cite{Krizhevsky2012}. Each of the 96 filters shown is of size $11\times11\times3$. The filters have clearly learned to detect edges of various orientations and they resemble Gabor filters. Image analysis using that kind of filters is thought to be similar to perception in the human visual system \cite{Marvcelja1980}.}
	\label{fig:alexnet_weights}
\end{figure}

Deep learning architectures usually consist of a multilayer stack of hierarchical learning modules which compute non-linear input-output mappings. Those modules are just functions of the input with a set of internal weights. The input of each layer in the stack is transformed, using the functions defined by the modules, to increase the selectivity and invariance of the representation. The backpropagation procedure is used to train those multilayer architectures by propagating gradients through all the modules. In the end, deep learning applications use feedforward neural network architectures which learn to map a fixed-size input, e.g., an image, to a fixed-size output, typically a vector containing a probability for each one of the possible categories \cite{Lecun2015}.

Figure \ref{fig:alexnet_weights} shows some sample filter modules automatically learned by training one of the most successful deep learning architectures: the deep convolutional neural network proposed by Krizhevsky et al. \cite{Krizhevsky2012} to classify the \num{1.2} million high-resolution images in the ImageNet LSVRC-2010 \cite{Berg2010} contest into \num{1000} different classes.

\subsection{\aclp{CNN} for Image Analysis}
\label{subsec:introduction:cnn_for_image_analysis}

In spite of the fact that these kind of architectures showed a huge potential for solving many computer vision problems, they were ignored by the computer vision community during the \num{1990}s. The main reason for that was twofold. It was widely accepted that learning feature extractors with little prior knowledge was impossible. In addition, most of the researchers thought that using simple gradient descent techniques to train those networks would get them inevitably trapped in poor local minima.

\begin{figure}[!b]
	\centering
	\input{Figures/related-works/convnet_architecture.tikz}
	%\caption[Illustration of the \acs{CNN} architecture proposed by krizhevsky et al.]{Illustration of the architecture of the aforementioned \ac{CNN} proposed by Krizhevsky et al.\cite{Krizhevsky2012} for the ImageNet challenge. Besides the normal components, e.g.,  convolutional, pooling, and fully connected layers, this network features two different paths. One \ac{GPU} runs the layers at the top while the other runs the layers at the bottom.}
	\caption{Illustration of the architecture of the aforementioned \ac{CNN} proposed by Krizhevsky et al.\cite{Krizhevsky2012} for the ImageNet challenge. Besides the normal components, e.g.,  convolutional, pooling, and fully connected layers, this network features two different paths. One \ac{GPU} runs the layers at the top while the other runs the layers at the bottom.}
	\label{fig:convnet_architecture}
\end{figure}

In the latter years, certain breakthrough works revived the interest in deep learning architectures \cite{Lecun2015}. Recent studies proved that local minima are not an issue with large neural networks. Following a set of seminal works for the field on training deep learning networks \cite{Hinton2006}\cite{Bengio2007}, a group of researchers from the \ac{CIFAR} introduced unsupervised learning procedures to create layers of feature detectors without labelled data, they also pre-trained several layers and added a final layer of output units; the system was tuned using backpropagation and achieved a remarkable performance when applied to the handwritten digit recognition or pedestrian detection problems \cite{Sermanet2013}. In addition, the advent of \acp{GPU}, which were easily programmable and extremely efficient for parallel problems, made possible the training of huge networks in acceptable time spans \cite{Raina2009}.

All those contributions to the field led to the birth of probably the most important milestone regarding deep learning: the \acf{CNN}. This special kind of deep network was designed to process data in form of multiple arrays and gained popularity because of its many practical successes. This was due to the fact that they were easier to train and generalized far better than previous models. The architecture of a typical \ac{CNN} is composed by many stages of convolutional layers followed by pooling ones and non-linearity \ac{ReLU} filters; in the end, convolutional and fully connected layers are stacked. The key idea behind using this stack of layers is to exploit the property that many natural signals are compositional hierarchies, in which higher-level features are obtained by composing lower-level ones. Figure \ref{fig:convnet_architecture} shows a typical architecture of a \ac{CNN}.

%\subsection{3D \aclp{CNN}}
%\label{subsec:introduction:3D_cnn}

%Due to the successful applications of the \acp{CNN} to \acs{2D} image analysis, several researchers decided to take another approach, the \ac{RGB-D} \ac{CNN}, by including the depth as an additional channel, along with the typical \ac{RGB} ones \cite{Socher2012}\cite{Alexandre2014}\cite{Hoeft2014}\cite{Lenz2015}. In spite of the fact that those methods extend the traditional \ac{CNN}, they do not employ a fully volumetric representation and therefore they do not make full use of the geometric information in the data. This is why they are called \acs{2.5D} \acp{CNN}.

%In order to improve the \acs{2.5D} \acp{CNN}, several authors proposed pure volumetric approaches or the so called \acs{3D} \acp{CNN} \cite{Wu2015}\cite{Maturana2015}\cite{Maturana2015a}. These architectures apply spatially \acs{3D} convolutions fully utilizing geometric data. However, by moving the convolutions to a pure \acs{3D} space those kind of methods do not take into account the \acs{RGB} information.

\section{Proposal and Goals}
\label{sec:introduction:proposal_goals}

In this work, we propose the implementation of a \acs{3D} object class recognition system using \acp{CNN}. That system will be accelerated using \acp{GPU} for both training and classification. The input of the system consists of segmented regions of point clouds which contain objects. Those point clouds will be synthetically simulated as if they were generated by low-cost \ac{RGB-D} sensors. To accomplish that proposal, we established a set of goals to be achieved:

\begin{itemize}
	\item \emph{Analyze the theoretical background of \acp{CNN}}. In order to implement our system, or extend existing frameworks to suit our needs, it is mandatory to understand the underlying principles of \acp{CNN}. For this purpose, we will review the core concepts and ideas that might be used during this work.
	\item \emph{Generate volumetric representations for \acs{3D} data}. This includes analyzing the state of the art for successful representations, proposing a suitable and efficient representation for our kind of data, implementing the means to generate them, and also choosing a proper dataset.
	\item \emph{Design, implement, and test a \acs{3D} \ac{CNN}}. As an intermediate step, \acs{2.5D} \acp{CNN} will be studied as well. To achieve this goal, the state of the art of both \acs{2.5D} and \acs{3D} \acp{CNN} will be reviewed. We will also analyze existing machine learning frameworks to determine the one that best suits our needs. In addition, a set of experiments will be carried out to analyze the accuracy and performance of the proposed architectures.
	\item \emph{Assemble and configure a server for training the system}. Given the significant computational needs of deep learning system during training, it becomes a matter of utmost importance to set up an appropriate server for experimenting with different configurations.
\end{itemize}

\section{Outline}
\label{sec:introduction:outline}

This document is structured as follows. Chapter \ref{cha:cnn} analyzes the theoretical background of \acp{CNN}. Chapter \ref{cha:materials_and_methods} describes the set of tools, data, and resources used in this work. Chapter \ref{cha:volumetric_representations} is devoted to volumetric representations for \acs{3D} data. Chapter \ref{cha:3dcnn_object_recognition} discusses and experiments with \acs{2.5D} and \acs{3D} \acp{CNN} for object recognition. At last, Chapter \ref{cha:conclusion} draws conclusions, shows the publications derived from this Thesis, and proposes future works.
