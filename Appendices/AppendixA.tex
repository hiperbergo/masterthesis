\chapter{Deep Learning Frameworks}
\label{app:deep-learning-frameworks}

Deep learning architectures have significantly influenced various application domains, surpassing by a large margin state-of-the-art methods for computer vision \cite{Krizhevsky2012, Russakovsky2015}, natural language processing \cite{Collobert2008, Collobert2011a, Glorot2011}, and speech recognition \cite{Deng2013, Deng2013a, Graves2013}. Due to that significant performance improvement achieved compared to traditional approaches, the popularity of deep learning methods has significantly increased over the last few years. Because of that, deep learning frameworks have also experienced a considerable growth in order to enable researchers to efficiently design, implement, and deploy these kind of architectures. Although this meteoric rise has hugely benefited the deep learning community, it also leads researchers to a certain confusion and difficulties to decide which framework to use. The fact is that different frameworks aim to optimize distinct aspects of the development process of deep learning architecture, i.e., choosing one framework over the others is mainly a matter of adequateness to the task at hand.

The list of available frameworks is vast and it is growing constantly as new frameworks are developed either to fill the gaps left by other's weaknesses or to provide new approaches and tools for researches to ease the development of deep architectures. Arguably, the most popular frameworks nowadays are Theano, Torch, Caffe, \ac{CNTK}, and TensorFlow. The list is not limited to those frameworks and also includes other less known ones such as DeepLearning4J, Neon, PyLearn, MXNet, ConvNet, CUDA-ConvNet, Deepmat, and more too numerous to mention. This appendix presents an overview of the five aforementioned most popular frameworks, as well as a brief comparison and discussion about their features to determine the best one for our purposes. In this regard, Table \ref{tab:deep-learning-frameworks} shows a quick comparison of the five discussed frameworks, taking into account certain high-level features. This brief comparison serves as a proper starting point to describe each one of the frameworks in depth throughout the next sections.

\begin{table}[!hbt]
	\centering
	\resizebox{\linewidth}{!}{
	\begin{tabular}{cccccc}
		Framework & License & Core & Interface & \acs{OpenMP} & \acs{CUDA}\\
		\hline
		Torch\cite{Collobert2011} & BSD 3-Clause & C, Lua & Torch, C, \CC{}/\acs{OpenCL} & Yes & Third Party\\
		Theano\cite{Bergstra2011} & BSD 3-Clause & Python & Python & Yes & Yes\\
		Caffe\cite{Jia2014} & BSD 2-Clause & \CC{}, Python & \CC{}, \acs{CLI}, Python, MATLAB & No & Yes\\
		\ac{CNTK}\cite{Agarwal2014} & MIT & \CC{} & \acs{CLI} & No & Yes\\
		TensorFlow\cite{Abadi2015} & Apache 2.0 & \CC{}, Python & C/\CC{}, Python & No & Yes\\
	\end{tabular}}
	\caption[Comparison of the most popular deep learning frameworks]{Comparison of the most popular deep learning frameworks, taking into account high-level features such as the source code license, the programming language used to write the core of the framework, the available interfaces to train, test, and deploy the models, and whether or not they support \ac{OpenMP} and \ac{CUDA}.}
	\label{tab:deep-learning-frameworks}
\end{table}

\section{Torch}

\emph{Torch}\cite{Collobert2011} is a scientific computing framework mainly maintained by researchers at \ac{FAIR} lab, Google DeepMind, Twitter, and also by a large list of contributors on \emph{GitHub}\footnote{\url{http://github.com/torch/torch7}}. It is a BSD-licensed C/Lua library that runs on Lua \ac{JIT} compiler with wide support for machine learning algorithms. Its goal is to have maximum flexibility and speed building scientific algorithms while making the process extremely simple thanks to an easy and fast scripting language, LuaJIT, and an underlying C/\ac{CUDA} implementation. One of its main highlights is its large ecosystem of community-driven packages and documentation.

The most important core features are easiness (it uses a simple scripting language as LuaJIT as an interface to the C core routines), efficiency (the underlying C/\ac{CUDA} implementation provides a strong backend for both CPU and \ac{GPU} computations with NVIDIA \ac{cuDNN}\cite{Chetlur2014}), optimized routines (for linear algebra, numeric optimization, and machine learning), and fast embedding with ports to iOS, Android and \ac{FPGA} backends.

\section{Theano}

\emph{Theano}\cite{Bergstra2010, Bergstra2011, Bastien2012} is a mathematical compiler for Python developed by a group of researches from the University of Montreal \ac{CSO} department, and maintained by them together with the contributions of a number users on \emph{GitHub}\footnote{\url{https://github.com/Theano/Theano}}. It is a BSD-licensed framework in the Python programming language  which can be used to define mathematical functions in a symbolic way, derive gradient expressions, and compile those expressions into executable functions for efficient performance in both CPU and \ac{GPU} platforms. It was conceived as a general mathematical tool, but its main goal was to facilitate research in deep learning.

The main features of Theano are the following ones: powerful tools for manipulating and optimizing symbolic mathematical expressions, fast to write and execute thanks to its dependency on NumPy\cite{VanDerWalt2011} and SciPy\cite{Jones2014}, \ac{CUDA} code generators for fast GPU execution of compiled mathematical expressions, and code stability and community support due to its short stable release cycle and exhaustive testing.

\section{Caffe}

\emph{Caffe}\cite{Jia2014} is a deep learning framework developed and maintained by the \ac{BVLC} and an active community of contributors on \emph{GitHub}\footnote{\url{http://github.com/BVLC/caffe}}. This BSD-licensed \CC{} library provides means to create, train, and deploy general-purpose \acp{CNN} and other deep models efficiently, mainly thanks to its drop-in integration of NVIDIA \ac{cuDNN}\cite{Chetlur2014} to take advantage of GPU acceleration.

The highlights of this framework are modularity (allowing easy extension by defining new data formats, layers, and functions), separation of representation and implementation (model definitions are written as configuration files in \emph{Protocol Buffer} language), test coverage (all modules have tests which have to be passed to be accepted into the project), Python/MATLAB bindings (for rapid prototyping, besides the \ac{CLI} and \CC{} \acs{API}), and pre-trained reference models.

\section{\acl{CNTK}}

The \ac{CNTK} \cite{Agarwal2014} is a general purpose machine learning framework developed by Microsoft Research and recently open-sourced under the MIT license on \emph{GitHub}\footnote{\url{https://github.com/Microsoft/CNK}}. Its main contribution is providing a unified framework for describing learning systems as \ac{CN}, and the means for training and evaluating that series of computational steps. Despite the fact that it was created as a general toolkit, it mainly focuses on deep neural networks, so it can be redefined as a deep learning tool that balances efficiency, performance, and flexibility. The core is written in \CC{} and the \ac{CN} models can be described in both \CC{} or \ac{NDL}/\ac{MEL}.

Thanks to the directed graph representation of the computation steps, it allows to easily recreate common models and extend them or create new ones from scratch by combining simple functions in arbitrary ways. It features a significantly modularized architecture, \ac{SGD} for training, auto-differentiation to obtain the gradient derivatives, and parallelization focused on \ac{GPU} clusters across multiple nodes with \ac{CUDA}-capable devices.

\section{TensorFlow}

TensorFlow\cite{Abadi2015} is a library for numerical computation using data flow graphs originally developed by researchers and engineers working on the Google Brain team for the purposes of conducting machine learning and deep neural networks research, currently an active community of \emph{GitHub} users contribute to the source code as well using its repository \footnote{\url{https://github.com/tensorflow/tensorflow}}. Its core is written in \CC{} and the whole framework is being open sourced under the Apache 2.0 license. The main goal of this project is to provide an interface, in both Python and \CC{}, for expressing artificial intelligence or machine learning algorithms, together with an efficient implementation of those algorithms which are flexible and scalable, i.e., the computations can be executed on a wide variety of devices with minimal or no changes at all.

This library features deep flexibility (every computation that can be expressed as a data flow graph can be implemented using TensorFlow), portability and scalability (training, testing, and deployment run on both CPUs and \acp{GPU} and they scale from single nodes to large-scale systems, besides it allows users to freely assign compute elements of the graph to different devices), research and production (deploying experimental systems is an easy task and requires minimal changes), and auto-differentiation (the derivatives needed for the data flow graph when expressing gradient based machine learning algorithms are computed automatically).
